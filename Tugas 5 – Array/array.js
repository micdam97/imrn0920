// Soal 1
function range(startNum = "kosong", finishNum = "kosong") {
    var array = []
    if(startNum == "kosong" || finishNum == "kosong") {
        return -1
    } else if(startNum < finishNum) {
        while(startNum <= finishNum) {
            array.push(startNum)
            startNum++;
        }
        return array
    } else {
        while(startNum >= finishNum) {
            array.push(startNum)
            startNum--;
        }
        return array
    }
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if(startNum < finishNum) {
        while(startNum <= finishNum) {
            array.push(startNum)
            startNum += step;
        }
    } else {
        while(startNum >= finishNum) {
            array.push(startNum)
            startNum -= step;
        }
    }
    return array
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
function sum(startNum = 0, finishNum = 0, step = 1) {
    hasil = 0
    if(startNum < finishNum) {
        for (i = startNum; i <= finishNum; i += step){
            hasil += i
        }
    } else {
        for (i = startNum; i >= finishNum; i -= step){
            hasil += i
        }
    }
    return hasil
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal 4
function dataHandling(array) {
    for (i = 0; i <= array.length-1; i++) {
        console.log("Nomor ID:  " + array[i][0])
        console.log("Nama Lengkap:  " + array[i][1])
        console.log("TTL:  " + array[i][2] + " " + array[i][3])
        console.log("Hobi:  " + array[i][4])
        console.log()
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)

// Soal 5
function balikKata(kalimat) {
    hasil = ""
    for (i=kalimat.length-1; i>=0; i--) {
        hasil += kalimat[i]
    }
    return hasil
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
function dataHandling2(array) {
    array.splice(1, 1, "Roman Alamsyah Elsharawy")
    array.splice(2, 1, "Provinsi Bandar Lampung")
    array.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(array)
    var tanggal = array[3].split("/")
    switch(tanggal[1]) {
        case "01": console.log("Januari"); break;
        case "02": console.log("Februari"); break; 
        case "03": console.log("Maret"); break; 
        case "04": console.log("April"); break; 
        case "05": console.log("Mei"); break; 
        case "06": console.log("Juni"); break; 
        case "07": console.log("Juli"); break; 
        case "08": console.log("Agustus"); break; 
        case "09": console.log("September"); break; 
        case "10": console.log("Oktober"); break; 
        case "11": console.log("November"); break; 
        case "12": console.log("Desember"); break;
    }
    console.log(tanggal.sort(function (value1, value2) { return value2 - value1 }))
    tanggal = array[3].split("/")
    tanggalBaru = tanggal.join("-")
    console.log(tanggalBaru)
    sliceNama = array[1].slice(0, 15)
    console.log(sliceNama)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);