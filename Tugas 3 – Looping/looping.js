// Soal 1
var i = 2;
console.log("LOOPING PERTAMA");
while(i <= 20){
    console.log(i + " - I Love coding");
    i += 2;
}

i = 20;
console.log("LOOPING KEDUA");
while(i >= 2){
    console.log(i + " - I will become  a mobile developer");
    i -= 2;
}

// Soal 2
for(i = 1; i <= 20; i++){
    if(i % 3 == 0 && i % 2 == 1){
        console.log(i + " - I Love Coding")
    } else {
        if(i % 2 == 1){
            console.log(i + " - Santai")
        } else {
            console.log(i + " - Berkualitas")
        }
    }
}

// Soal 3
for (i = 1; i <= 4; i++){
    for(j = 1; j <= 8; j++){
        process.stdout.write("#")
    }
    console.log()
}

// Soal 4
for (i = 1; i <= 7; i++){
    for(j = 1; j <= i; j++){
        process.
        stdout.write("#")
    }
    console.log()
}

// Soal 5
for (i = 1; i <= 8; i++){
    if (i % 2 == 1){
        console.log(" # # # #")
    } else {
        console.log("# # # # ")
    }
}