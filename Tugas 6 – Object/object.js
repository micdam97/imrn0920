// Soal 1
function arrayToObject(arr) {
    if(arr[0] == null) {
        return console.log("")
    } else {
        var now = new Date()
        var thisYear = now.getFullYear() 
        for(var i=0; i<=arr.length-1; i++){
            var firstName = arr[i][0]
            var lastName = arr[i][1]
            var gender = arr[i][2]
            if(arr[i][3] > thisYear || arr[i][3] == null){
                age = "Invalid Birth Year"
            } else {
                age = thisYear - arr[i][3]
            }
            var obj = {
                firstName: firstName,
                lastName: lastName,
                gender: gender,
                age: age
            }
            process.stdout.write([i+1] + ". " + firstName + " " + lastName + ": ")
            console.log(obj)
        }
    }
}
 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
arrayToObject([])

// Soal 2
function shoppingTime(memberId = "", money) {
    if (memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var listPurchased = []
        var changeMoney = money
        while(changeMoney > 5000) {
            if(changeMoney >= 1500000) {
                listPurchased.push("Sepatu Stacattu")
                changeMoney -= 1500000
            } else if(changeMoney >= 500000) {
                listPurchased.push("Baju Zoro")
                changeMoney -= 500000
            } else if(changeMoney >= 250000) {
                listPurchased.push("Baju H&N")
                changeMoney -= 250000
            } else if(changeMoney >= 175000) {
                listPurchased.push("Sweater Uniklooh")
                changeMoney -= 175000
            } else {
                listPurchased.push("Casing Handphone")
                changeMoney -= 50000
                break
            }
        }
        hasil = {
            memberId: memberId,
            money: money,
            listPurchased: listPurchased,
            changeMoney: changeMoney
        }
        return hasil
    }
  }
   
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); 
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime()); 

// Soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(arrPenumpang[0] == null) {
        return []
    } else {
        var hasil = []
        for(i=0; i<arrPenumpang.length; i++) {
            var penumpang = arrPenumpang[i][0]
            var naikDari = arrPenumpang[i][1]
            var tujuan = arrPenumpang[i][2]
            var bayar = (rute.indexOf(tujuan) - rute.indexOf(naikDari)) * 2000
            var obj = {
                penumpang: penumpang,
                naikDari: naikDari,
                tujuan: tujuan,
                bayar: bayar
            }
            hasil.push(obj)
        }
        return hasil
    }
  }
  
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
   
  console.log(naikAngkot([])); //[]