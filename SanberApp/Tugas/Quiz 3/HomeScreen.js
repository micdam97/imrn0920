import React from 'react';
import { Text, View, StyleSheet} from 'react-native';

const homeScreen = ({
    params,
}) => {
    <View style={styles.home}>
        <Text>Home Screen</Text>
    </View>
};
const styles = StyleSheet.create({
    home : {
        flex: 1,
        justifyContent: 'center',
        alighItems: 'center',
    }
});
export default homeScreen;