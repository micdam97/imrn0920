var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
time = 10000
i = 0

function timetoRead() {
    readBooksPromise(time, books[i])
        .then ((resolve) => {
            time = resolve
            i++
            if (i < books.length) {
                timetoRead()
            }
        })
}

timetoRead()
